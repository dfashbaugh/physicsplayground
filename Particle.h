#include <SDL.h>

#ifndef PARTICLE_H
#define PARTICLE_H

class Particle
{
    public:
        Particle(int xPos, int yPos, int XVel, int YVel);
        ~Particle();

        // Make the particle fall
        void Move       ();
        bool isDestroy  ();

        void render     (SDL_Renderer* renderer);
    protected:

        static const int PARTICLE_MAX_VEL = 45;

        // Acceleration of gravity and "drag"
        int mYAcceleration;
        int mXAcceleration;

        // Current particle Velocity
        int mYVel;
        int mXVel;

        // Current Particle Position
        int mYPos;
        int mXPos;

        // Particle Piece
        SDL_Rect    mParticle;

        // Particle Color
        int         mParticleColor;

        bool        mParticleStartRight;

    private:
};

#endif // PARTICLE_H
