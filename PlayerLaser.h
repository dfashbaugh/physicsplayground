#include <SDL.h>

#ifndef PLAYERLASER_H
#define PLAYERLASER_H


class PlayerLaser
{
    public:
        PlayerLaser(int posX, int posY);
        ~PlayerLaser();

        void    Move    ();
        void    Render  (SDL_Renderer* renderer);

        bool        getDestroy  ();

        SDL_Rect    getHitBox   ();

    protected:

        // Laser Shape
        SDL_Rect    mLaserShape;

        int     mPosX;
        int     mPosY;

        int     mVelX;
        int     mVelY;
    private:
};

#endif // PLAYERLASER_H
