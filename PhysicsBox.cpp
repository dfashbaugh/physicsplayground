#include <Box2D/Box2D.h>
#include "globals.h"
#include "PhysicsBox.h"

PhysicsBox::PhysicsBox(double posX, double posY, double angle, double width, double height, double density, double friction)
{
    //ctor
    mBodyDef = new b2BodyDef();
    mBodyDef->type = b2_dynamicBody;
    mBodyDef->position.Set(posX, posY);
    mBodyDef->angle = angle;

    mDynamicBox.SetAsBox(width, height);

    mFixtureDef.shape = &mDynamicBox;
    mFixtureDef.density = density;
    mFixtureDef.friction = friction;

    mpBody = NULL;
}

PhysicsBox::~PhysicsBox()
{
    //dtor
}

void PhysicsBox::render(SDL_Renderer* renderer, LTexture& theTexture)
{
    b2Vec2 position = mpBody->GetPosition();
    float32 angle = -1 * ((mpBody->GetAngle()*180)/3.14159);

    int pixelX = position.x*PIXEL_PER_METER_RATIO;
    int pixelY = SCREEN_HEIGHT - (position.y*PIXEL_PER_METER_RATIO);

    theTexture.render( renderer, pixelX, pixelY, NULL, angle  );
}

void PhysicsBox::intializeBody(b2Body* body)
{
    mpBody = body;
    mpBody->CreateFixture(&mFixtureDef);
}

void PhysicsBox::handleEvent( SDL_Event& e)
{
    //If a key was pressed
	if( e.type == SDL_KEYDOWN )
    {
        //Adjust the velocity
        const b2Vec2 Up(0, 50);
        const b2Vec2 Down(0, -50);
        const b2Vec2 Left(-50, 0);
        const b2Vec2 Right(50, 0);
        switch( e.key.keysym.sym )
        {
            case SDLK_LEFT: mpBody->ApplyForce( Left, mpBody->GetWorldCenter(), true ); break;
            case SDLK_RIGHT: mpBody->ApplyForce( Right, mpBody->GetWorldCenter(), true ); break;
        }

        switch( e.key.keysym.sym )
        {
            case SDLK_UP: mpBody->ApplyForce( Up, mpBody->GetWorldCenter(), true ); break;
            case SDLK_DOWN: mpBody->ApplyForce( Down, mpBody->GetWorldCenter(), true ); break;
        }
    }
    //If a key was released
    else if( e.type == SDL_KEYUP && e.key.repeat == 0 )
    {

    }
}
