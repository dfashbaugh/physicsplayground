#include "EnemyShip.h"
#include <stdio.h>
#include "globals.h"

EnemyShip::EnemyShip(int posX, int posY)
{
    //ctor

    mHitBox.h = 20;
    mHitBox.w = 20;
    mHitBox.x = posX;
    mHitBox.y = posY;

    mPosX = posX;
    mPosY = posY;

    mVelX = 20;
    mVelY = 20;

    mAnimationState = moveRight;

    mMoveRightLeftCounter = 0;
}

EnemyShip::~EnemyShip()
{
    //dtor
}

void EnemyShip::Move(int vSyncAnimationCounter)
{
    if(vSyncAnimationCounter == VSYNC_ANIMATION_MAX)
    {
        if(mMoveRightLeftCounter >= MOVE_RIGHT_LEFT_COUNT_MAX)
        {
            mPosY += mVelY;
            mMoveRightLeftCounter = 0;

            if(mAnimationState == moveRight)
            {
                mAnimationState = moveLeft;
            }
            else
            {
                mAnimationState = moveRight;
            }
        }
        else
        {
            if(mAnimationState == moveRight)
            {
                mPosX += mVelX;
                mMoveRightLeftCounter++;
            }
            else if(mAnimationState == moveLeft)
            {
                mPosX -= mVelX;
                mMoveRightLeftCounter++;
            }
        }

    }

    mHitBox.x = mPosX;
    mHitBox.y = mPosY;
}

void EnemyShip::Render(SDL_Renderer* renderer)
{
    mHitBox.x = mPosX;
    mHitBox.y = mPosY;
    SDL_SetRenderDrawColor( renderer, 0xFF, 0xFF, 0x00, 0xFF);
    SDL_RenderFillRect( renderer, &mHitBox);
}
