#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <vector>
#include "LTexture.h"
#include "PlayerLaser.h"
#include "EnemyShip.h"
#include "PlayerShip.h"
#include "Particle.h"

#ifndef GAMEGRAPHICS_H
#define GAMEGRAPHICS_H

class GameGraphics
{
    public:
        GameGraphics();
        virtual ~GameGraphics();

        bool InitializeWindow();
        bool LoadMedia();
        void ShutdownWindow();
        void RunLoop();

    protected:

        LTexture mPlayerShipTexture;
        LTexture mWinnerTexture;

        SDL_Window* mWindow;
        SDL_Renderer* mRenderer;

        int     mVSyncAnimationCounter;

        double  mPhysicsBoxWidth;
        double  mPhysicsBoxHeight;

        int     mScore;
        bool    mPlayerWon;
    private:
};

#endif // GAMEGRAPHICS_H
