//The dimensions of the level
const int LEVEL_WIDTH = 800;
const int LEVEL_HEIGHT = 600;

//Screen dimension constants
const int SCREEN_WIDTH = 1440;
const int SCREEN_HEIGHT = 900;

const int VSYNC_ANIMATION_MAX = 50;
const int MOVE_RIGHT_LEFT_COUNT_MAX = 5;

const double PIXEL_PER_METER_RATIO = 80;
