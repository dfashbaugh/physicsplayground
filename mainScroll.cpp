
/*This source code copyrighted by Lazy Foo' Productions (2004-2014)
and may not be redistributed without written permission.*/

//Using SDL, SDL_image, standard IO, vectors, and strings
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include "LTexture.h"
#include "GameGraphics.h"

int main( int argc, char* args[] )
{
    GameGraphics theGraphics;

    printf("About to initialize window\n");
    if( !theGraphics.InitializeWindow() )
    {
        printf( "Failed to initialize!\n" );
    }
    else
    {
        printf("About to load media\n");
        if( !theGraphics.LoadMedia() )
        {
            printf( "Failed to load media!\n" );
        }
        else
        {
            printf( "About to run loop\n");
            theGraphics.RunLoop();
        }
    }

    printf("About to Exit Program\n");
    theGraphics.ShutdownWindow();

	return 0;
}
