#include <stdlib.h>
#include "globals.h"
#include "Particle.h"

Particle::Particle(int xPos, int yPos, int XVel, int YVel)
{
    //ctor
    mParticle.x = xPos;
    mParticle.y = yPos;
    mParticle.w = 10;
    mParticle.h = 10;

    mYAcceleration = 1; // Gravity
    mXAcceleration = 0; // Drag

    mParticleStartRight = false;
    if(XVel > 0)
    {
        mXAcceleration = mXAcceleration*-1;
        mParticleStartRight = true;
    }

    mYVel = YVel;
    mXVel = XVel;

    mYPos = yPos;
    mXPos = xPos;

    mParticleColor = rand()%4;
}

Particle::~Particle()
{
    //dtor
}

void Particle::Move()
{
    mYVel += mYAcceleration;
    mXVel += mXAcceleration;

    // Make particle fall straight
    if(mParticleStartRight)
    {
        if(mXVel < 0)
        {
            mXVel = 0;
        }
    }
    else
    {
        if(mXVel > 0)
        {
            mXVel = 0;
        }
    }

    if(mYVel > PARTICLE_MAX_VEL)
    {
        mYVel = PARTICLE_MAX_VEL;
    }

    mXPos += mXVel;
    mYPos += mYVel;

    mParticle.x = mXPos;
    mParticle.y = mYPos;
}

bool Particle::isDestroy()
{
    if(mYPos > SCREEN_HEIGHT)
    {
        return true;
    }

    return false;
}

void Particle::render(SDL_Renderer* renderer)
{
    if(mParticleColor == 0)
    {
        SDL_SetRenderDrawColor( renderer, 0xFF, 0x00, 0xFF, 0xFF);
    }
    else if(mParticleColor == 1)
    {
        SDL_SetRenderDrawColor( renderer, 0xFF, 0x00, 0x00, 0xFF);
    }
    else if(mParticleColor == 2)
    {
        SDL_SetRenderDrawColor( renderer, 0xFF, 0xFF, 0xFF, 0xFF);
    }
    else
    {
        SDL_SetRenderDrawColor( renderer, 0xFF, 0xFF, 0x00, 0xFF);
    }
    SDL_RenderFillRect( renderer, &mParticle);
}
