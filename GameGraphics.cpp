#include "GameGraphics.h"
#include "LTexture.h"
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <stdlib.h>
#include "globals.h"
#include "PlayerLaser.h"
#include "EnemyShip.h"
#include "PlayerShip.h"
#include "PhysicsBox.h"
#include <vector>
#include <Box2D/Box2D.h>



GameGraphics::GameGraphics()
    : mPlayerShipTexture()
    , mWinnerTexture()
    , mVSyncAnimationCounter (0)
    , mPhysicsBoxWidth (0.5f)
    , mPhysicsBoxHeight (0.5f)
    , mScore(0)
    , mPlayerWon(false)
{
    //ctor
}

GameGraphics::~GameGraphics()
{
    //dtor
}

void GameGraphics::RunLoop()
{
    // Initialize the Physics
	// Define the gravity vector.
	b2Vec2 gravity(0.0f, -10.0f);

	// Construct a world object, which will hold and simulate the rigid bodies.
	b2World world(gravity);

	// Define the ground body.
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0.0f, -10.0f);

	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
	b2Body* groundBody = world.CreateBody(&groundBodyDef);

	// Define the ground box shape.
	b2PolygonShape groundBox;

	// The extents are the half-widths of the box.
	groundBox.SetAsBox(50.0f, 10.0f);

	// Add the ground fixture to the ground body.
	groundBody->CreateFixture(&groundBox, 0.0f);


	// Prepare for simulation. Typically we use a time step of 1/60 of a
	// second (60Hz) and 10 iterations. This provides a high quality simulation
	// in most game scenarios.
	float32 timeStep = 1.0f / 60.0f;
	int32 velocityIterations = 6;
	int32 positionIterations = 2;

    std::vector <PhysicsBox*> boxes;
/*
    boxes.push_back(new PhysicsBox(4.5f, 8.0f, 0.0f, mPhysicsBoxWidth, mPhysicsBoxHeight, 0.50f, 0.3f) );
    boxes.push_back(new PhysicsBox(4.0f, 4.0f, 0.0f, mPhysicsBoxWidth, mPhysicsBoxHeight, 1.0f, 0.3f) );

    boxes.push_back(new PhysicsBox(3.0f, 8.0f, 0.7f, mPhysicsBoxWidth, mPhysicsBoxHeight, 0.50f, 0.3f) );
    boxes.push_back(new PhysicsBox(4.0f, 4.0f, 0.0f, mPhysicsBoxWidth, mPhysicsBoxHeight, 1.0f, 0.3f) );
    boxes.push_back(new PhysicsBox(2.0f, 8.0f, 0.0f, mPhysicsBoxWidth, mPhysicsBoxHeight, 1.0f, 0.3f) );
    boxes.push_back(new PhysicsBox(2.0f, 4.0f, 1.57f, mPhysicsBoxWidth, mPhysicsBoxHeight, 1.0f, 0.3f) );
    boxes.push_back(new PhysicsBox(3.0f, 4.0f, 3.14f, mPhysicsBoxWidth, mPhysicsBoxHeight, 1.0f, 0.3f) );
    boxes.push_back(new PhysicsBox(1.0f, 4.0f, 4.71f, mPhysicsBoxWidth, mPhysicsBoxHeight, 1.0f, 0.3f) );
    boxes.push_back(new PhysicsBox(3.0f, 8.0f, 0.0f, mPhysicsBoxWidth, mPhysicsBoxHeight, 1.0f, 0.3f) );
    boxes.push_back(new PhysicsBox(3.0f, 10.0f, 0.0f, mPhysicsBoxWidth, mPhysicsBoxHeight, 1.0f, 0.3f) );
    boxes.push_back(new PhysicsBox(4.0f, 10.0f, 0.0f, mPhysicsBoxWidth, mPhysicsBoxHeight, 1.0f, 0.3f) );

*/
    for(int i = 2; i < 12; i++)
    {
        for(int j = 3; j<13; j++)
        {
            double spot1 = i;
            double spot2 = j;
            boxes.push_back(new PhysicsBox(spot1, spot2, 0.7f, mPhysicsBoxWidth, mPhysicsBoxHeight, 0.50f, 0.3f) );
        }
    }

    for(int i = 0; i < (int)boxes.size(); i++)
    {
        boxes[i]->intializeBody( world.CreateBody( boxes[i]->getBodyDef() ) );
    }
    // Finish the physics

	//Main loop flag
	bool quit = false;

	//Event handler
	SDL_Event e;

    //fillEnemyShipVector();

	//While application is running
	while( !quit )
	{
		//Handle events on queue
		while( SDL_PollEvent( &e ) != 0 )
		{
			//User requests quit
			if( e.type == SDL_QUIT)
			{
				quit = true;
			}

            for(int i = 0; i < (int)boxes.size(); i++)
            {
                boxes[i]->handleEvent(e);
            }
		}

        // Instruct the world to perform a single step of simulation.
		// It is generally best to keep the time step and iterations fixed.
		world.Step(timeStep, velocityIterations, positionIterations);

        //Clear screen
        SDL_SetRenderDrawColor( mRenderer, 0x00, 0x00, 0x00, 0x00 );
        SDL_RenderClear( mRenderer );

        for(int i = 0; i < (int)boxes.size(); i++)
        {
            boxes[i]->render(mRenderer, mPlayerShipTexture);
        }

        //Update screen
        SDL_RenderPresent( mRenderer );
	}
}

bool GameGraphics::InitializeWindow()
{
//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Set texture filtering to linear
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		mWindow = SDL_CreateWindow( "Physics Box", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( mWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			//Create vsynced renderer for window
			mRenderer = SDL_CreateRenderer( mWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
			if( mRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor( mRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

// Load Textures
bool GameGraphics::LoadMedia()
{
	//Loading success flag
	bool success = true;

	//Load player ship texture
	int boxWidth = mPhysicsBoxWidth*PIXEL_PER_METER_RATIO*2;
    int boxHeight = mPhysicsBoxHeight*PIXEL_PER_METER_RATIO*2;
	printf("Box width %d; Box height %d\n", boxWidth, boxHeight);

	if( !mPlayerShipTexture.loadFromFile(mRenderer, "Block.bmp", boxWidth, boxHeight ) )
	{
		printf( "Failed to load ship texture!\n" );
		success = false;
	}

	// Load you won texture
	if( !mWinnerTexture.loadFromFile(mRenderer, "YouWon.bmp" ) )
	{
		printf( "Failed to load You Won! texture!\n" );
		success = false;
	}

	return success;
}

// Shutdown the graphics
void GameGraphics::ShutdownWindow()
{
	//Destroy window
	SDL_DestroyRenderer( mRenderer );
	SDL_DestroyWindow( mWindow );
	mWindow = NULL;
	mRenderer = NULL;

	mPlayerShipTexture.free();
	mWinnerTexture.free();

	//Quit SDL subsystems
	IMG_Quit();
	SDL_Quit();
}

