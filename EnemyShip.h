#include <SDL.h>

#ifndef ENEMYSHIP_H
#define ENEMYSHIP_H


class EnemyShip
{
    public:
        EnemyShip(int posX, int posY);
        ~EnemyShip();

        void        Move        (int vSyncAnimationCounter);
        void        Render      (SDL_Renderer* renderer);

        SDL_Rect    getHitBox   ()      {return mHitBox; };

        int         getXPos     ()      {return mHitBox.x; };
        int         getYPos     ()      {return mHitBox.y; };

    protected:

        enum animationState {moveRight, moveLeft, moveDown};

        animationState mAnimationState;

        int mMoveRightLeftCounter;

        int mPosX;
        int mPosY;

        int mVelX;
        int mVelY;

        SDL_Rect    mHitBox;
    private:
};

#endif // ENEMYSHIP_H
