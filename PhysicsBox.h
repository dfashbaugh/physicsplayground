#include <SDL.h>
#include <Box2D/Box2D.h>
#include "LTexture.h"

#ifndef PHYSICSBOX_H
#define PHYSICSBOX_H


class PhysicsBox
{
    public:
        PhysicsBox(double posX, double posY, double angle, double width, double height, double density, double friction); // values in meters. The position is relative to the ground
        ~PhysicsBox();

        b2BodyDef*   getBodyDef      ()                  {return mBodyDef; };
        void        intializeBody   (b2Body* body);
        void render (SDL_Renderer* renderer, LTexture& theTexture);
        //Takes key presses and adjusts the dot's velocity
		void handleEvent( SDL_Event& e);

    protected:

        double mPosX;
        double mPosY;
        double mAngle;

        SDL_Rect    mRenderRect;

        b2BodyDef*      mBodyDef;  // Dynamic body definition
        b2PolygonShape  mDynamicBox;
        b2FixtureDef    mFixtureDef;
        b2Body*         mpBody;

    private:
};

#endif // PHYSICSBOX_H
